GitLet
===
- some from: http://stackoverflow.com/questions/14011046/what-happens-to-source-and-destination-branches-after-git-merge
- some from: http://www-inst.eecs.berkeley.edu/~cs61bl/su15/materials/proj/proj2/proj2.html

A version control system is essentially a **backup system for files** on my computer. The main functionality that gitlet supports 

1. **Saving** backups of files (**Committing**)
2. **Resotring** a backup version of a file (**Checking out**)
3. **Viewing** the history of my backups. (**Log**)

#### Visualization of commits
![Image of subsequent commits](https://github.com/eunjincho503/GitLet/blob/master/readme_img/commit_list.png)
- We can visualize the history of the different versions of our files in a **list**. We call the commit that came before it the **parent commit**.
- If we tell gitlet to revert to an old commit, we introduce the **head pointer**. The head pointer keeps track of where in the linked list we're currently **at**. 
    - Normally, as we make commits, the head pointer will stay at the **front** of the linked list.
    ![Image of normal commit](https://github.com/eunjincho503/GitLet/blob/master/readme_img/revert_list.png)
    - If we revert to the state of the files at commit #2, we move the head pointer.
    ![Image of reverting back to old commit](https://github.com/eunjincho503/GitLet/blob/master/readme_img/revert_list2.png)

#### Maintaining differing versions
- Imagine I'm coding a project, and I have 2 ideas about how to proceed: Plan A and Plan B. 
- GitLet allows us to **save both versions** and **switch between them at will**.
![Image of differing](https://github.com/eunjincho503/GitLet/blob/master/readme_img/both_versions.png)
- It more like a **tree** (commit tree). Notice there are 2 pointers int othe tree. At any given time, only one of these is the currently active pointer = **head pointer** = the pointer at the front of the current branch.
- This tree is **immutable**. So once a commit node has been created, it can never be destroyed (or changed at all).  We can only add new things to the commit tree.

 
## Gitlet's must satisfy List
1. Gitlet should never delete files
2. It will need a place to store old copies of files, and other metadata. All of this stuff **must** be stored in a folder called ```.gitlet``` (hidden file). So a gitlet system is considered "initialized" in a particular location if it has a ```.gitlet``` folder there. Most gitlet commands(except for the ```init``` command) only work when used from a directory where a gitlet system has been initialized (a directory that has a ```.gitlet``` folder) --> the files that aren't in ```.gitlet``` folder (current versions of the files, not the backups) are referred to as the **working directory**.
3. Most commands have runtime or memory usage requiremnets. Significant measures: # files/ # commits. Not significant measures: time to serialize or deserialize (BUT **serialization time cannot depend on the total size of files that have been added, committed etc**)
4. Some commands have failure cases with a specific error message.
5. Failure cases that don't apply to a particular command: If a user doesn't input any argument/ If a user inputs a command that doesn't exist.
6. Some commands might be "dangerous" - if a user tells gitlet to restore files to older versions, gitlet may **overwrite** the current versions of the files.

## Commands

![Image of differing](https://github.com/eunjincho503/GitLet/blob/master/readme_img/git_process.png) 
![Image of differing](https://github.com/eunjincho503/GitLet/blob/master/readme_img/git_process2.png) 


### init
- How to use: ```java Gitlet init```

- Description:
    - Creates a new gitlet version control system in the current directory. 
    - This system will automatically start with 1 commit : a commit that contains no file and has the commit msg ```initial commit```

- Failure cases: 
    - If there's already a gitlet version control system in the current directory, it should abort.
    - should NOT overwrite the existing system with a new one


### add
- How to use: ```java Gitlet add [filename]```

- Description:
    - Adds a copy of the file as it currently exists to the **staging area**.(Adding a file = Staging the file)
    - The staging area should be somewhere in ```.gitlet```. 
    - If the file had been **marked for untracking** (```rm``` method), then ```add``` just **unmarks** the file, and doesn't add it to the staging area.

- Failure cases: 
    - If the file does not exist, print error msg.


### commit
- How to use: ```java Gitlet commit [message]```
- Description:
    - Saves a backup of a certain files so they can be restored at a later time.
    - The collection of versions of files in a commit = commit's *snapshot* of files = commit is tracking those versions of files
    - A commit will only update the version of a file if that file had been staged at the time of commit (the commit will now include the version of the file that was staged instead of the version it got from its parent)
    - A commit will start **tracking any files that were staged** but weren't tracked by its parent.
    - ==> By default, **a commit is the same as its parent. Staged files are the updates to the commit**

- Additional points:
    - Files that were marked for **untracking** lose this mark after a commit. 
    - The staging area is cleared after a commit. (delete files)
    - Since the staging area is insided the ```.gitlet``` folder, gitlet will only ever delete files inside ```.gitlet```. Gitlet should never ever delete files outside of ```.gitlet```.
    - After the commit command, the new commit is added as a new node in the commit tree.
    - The commit just made becomes the "current commit", and the head pointer now points to it. The previous head commit now becomes the current commit's parent commit.
    - Each commit should have a unique integer id number
    - Each commit should remember what time it was made.
    - Each commit has a message associated with it that describes the changes to the files in the commit. This is specified by the user. 

- Failure cases:
    - If no files have been staged(added)+(or marked for untracking), aborts 
    - Every commit must have a non-blank message.
    - If there's a file marked for **untracking** that **isn't in the current commit** = is NOT a failure case (this can happen if you call ```rm``` and then move the head pointer before committing)
![Image of differing](https://github.com/eunjincho503/GitLet/blob/master/readme_img/before_after_commit.png)


### remove
- How to use: ```java Gitlet rm [filename]```

- Description:
    - Mark the file for **untracking**. It means it will not be included in the upcoming commit, even if it was tracked by that commit's parent
    - If the file had been staged, unstage it + do not also mark it for untracking
    - ```To remove a file from Git, you have to remove it from your tracked files (more accurately, remove it from your staging area) and then commit```
    - This command is more similar to ```git rm --cached [file name]``` in the real git.

- Failure cases:
    - if the file is not staged/ not tracked by the head commit ==> print error msg


### log
- How to use: ```java Gitlet log```

- Description:
    - Starting at the current head commit, **display** information about each commit **backwords** along the commit tree until the initial commit. (set of commit nodes = commit's **history**)
    - For every node in this history, the info it should display:
        - the commit id
        - the time the commit was made
        - the commit message
        - separated by ```===``` and an empty line
    - The most recent at the top 
![Image of log](https://github.com/eunjincho503/GitLet/blob/master/readme_img/log.png)


### global-log
- How to use: ```java Gitlet global-log```

- Description:
    - Like log, except displays information about all commits ever made.


### find
- How to use: ```java Gitlet find [commit message]```

- Description:
    - Prints out the id of the commit that has the given message
    - If there are multiple such commits, it prints the ids out on separate lines

- Failure cases:
    - if no such commit exists


### status
- How to use: ```java Gitlet status```

- Description:
    - Displays what branches currently exist and marks the current branch with a ```*```
    - Displays what files have been **staged** or **marked for untracking**
    - Separated by ```===[contents]===```


### checkout
- How to use: 
    - ```java Gitlet checkout [filename]```
    - ```java Gitlet checkout [commit id][filename]```
    - ```java Gitlet checkout [branchname]```

- Description:
    - Switch the currently active head pointer with ```checkout [branchname]```
    - Takes the version of the file as it exists in the head commit(the front of the current branch) and puts it in the working directory, overwriting the version of the file that's already there if there's one
    - Takes the version of the file as it exists in the commit with the given id and puts it in the working directory, overwriting the version of the file that's already there if there's one
    - Takes all files in the commit at the head of the given branch and puts them in the working directory, overwriting the versions of the files that are already there if they exist. At the end of this command, **the given branch will now be considered the current branch**.
    - 짧게 말하면, 3번째는 [branchname]에 속해있는 파일을 다 가져와서 working directory에 넣는 것 (이름이 같은 filename이 있으면 override하고 아니면 그냥 add). 
    - If file name and branch name are the same, let the branch name take precedence.
    - **Dangerous**

- Failure cases:
    - If the file does not exist in the previous commit, aborts
    - If no commit with the given id exists
    - If no branch with that name exists


### branch
- How to use: ```java Gitlet branch [branchname]```

- Description:
    - Creates a new branch with the given name and points it at the current head node
    - A branch is nothing more than a name for a pointer to a commit node into the commit tree
    - By default, my code is running with a default branch called ```master```
    - Do NOT immediately switch to the newly created branch
    - Creating a branch is give us a new pointer ```branch``` ==> We can switch the currently active head pointer with ```checkout``` ==> whenever we ```commit```, it means we add a new commit in front of the currently active head pointer

- Failure cases:
    - If a branch with the given name already exists
![Image of branch1](https://github.com/eunjincho503/GitLet/blob/master/readme_img/branch1.png)
![Image of branch2](https://github.com/eunjincho503/GitLet/blob/master/readme_img/branch2.png)


### rm-branch
- How to use: ```java Gitlet rm-branch [branchname]```

- Description:
    - Deletes the branch with the given name
    - Only means to delete the pointer associated with the branch
    - it does NOT mean to delete all commits that were created under the branch

- Failure cases:
    - If a branch with the given name does not exist
    - If you try to remove the branch you're currently on


### reset
- How to use: ```java Gitlet reset [commit id]```

- Description:
    - Checks out all the files tracked by the given commit
    - Also moves the current branch's head to that commit node
    - **Dangerous** because it throws away all the uncommitted changes
    - Make your current branch (typically master) back to point at ```commit id```
    - Then make the files in your working tree and the index ("staging area") the **same** as the versions committed in ```commit id```

- Failure cases:
    - If no commit with the given id exists

### merge
- How to use: ```java Gitlet merge [branchname]```
- Description1:
    - **Dangerous**
    - Merges files from the given branch into the current branch
    - First consider what might be called the **split point** of the current branch and the given branch (earliest **common** ancestor). Let's say the split point is ```B``` for two new versions ```X``` and ```Y```.
    ![Image of merge](https://github.com/eunjincho503/GitLet/blob/master/readme_img/merge1.png)
    - Perform diffs of ```X``` with ```B``` and ```Y``` with ```B```.
    - Walk through the changed blocks identified in the two diffs. If both sides introduce the same change in the same spot, accept either one. If one introduces a change and the other leaves that region unchanged, introduce the change in the final. If both introduce changes in a spot, but they don't match, mark a conflict to be resolved manually.
    - These files should then all be automatically staged (or marked for untracking, if the modification was a removal).

- Description 2:
    - Assume the following history exists and the current branch is "master":
```
          A---B---C topic
         /   
    D---E---F---G *master
```
    - Then ```git merge topic``` will replay the changes made on the ```topic``` branch since it diverged from master (i.e., E) until its current commit (C) on top of ```master```, and record the result in a new commit along with the names of the two parent commits and a log message from the user describing the changes.
```
          A---B---C topic
         /         \
    D---E---F---G---H master
```

- Description 3 (Should follow this):
    - First find the split point
    - If current branch has **not modified files** and given branch has **modified files** since split point, current branch's non-modified files should be changed to modified files in given branch.
    - These files should be staged (or marked for untracking if the modification was a removal). 
    - If current branch has **modified files** and the given branch has **not modifed files**, they should stay as they are.
    - For files that have been modified in both branches since split point, the files should stay as they are. However, the version of the file from the **given branch** should be copied into the working directory with the name ```[old file name].conflicted```.
    - Special case: If a file has been **untracked** between commits in a branch, this is considered a **modification** to the file.
    - Once files have been updated
        - If ```merge``` did not just generate any **.conflicted files**, then merge should automatically ```commit``` with a message ```Merged [current branch name] with [given branch name]```
        - If ```merge``` generated at least one **.conflicted file**, then merge should **NOT** automatically make a commit. Instead, it should print the message ```Encountered a merge conflict.``` and then put gitlet into a **conflicted state**. During a conflicted state, some commands are not allowed. The only commands allowed are ```add```, ```rm```, ```commit```, ```checkout [file]```, ```checkout [id] [file]```, ```log```, ```global-log```, ```find```, and ```status```.
        - Conflicted state ends once the user commits.

- Failure cases:
    - If a branch with the given name does not exist
    - If attempting to merge a branch with itself
    - If merge would generate an error because the commit that it does has no changes in it


### rebase 

- How to use: ```java Gitlet rebase [branchname]```
- Description:
    - **Dangerous**
    - Find the split point of the ```current branch``` and the ```given branch```, then **snaps off** the ```current branch``` at this point, then reattaches the ```current branch``` to the head of the ```given branch```.
    ![Image of rebase](https://github.com/eunjincho503/GitLet/blob/master/readme_img/rebase.png)
    - Technically, rebase does not break off the ```current branch``` like above. Instead, it leaves the ```current branch``` there but makes a copy of the ```current branch``` on top of the ```given branch```. Then it moves the **branch pointer** to point to this copy. These commits should have **new ids** and **new time stamps**s.
    ![Image of rebase2](https://github.com/eunjincho503/GitLet/blob/master/readme_img/rebase2.png)


- Failure cases:
