import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

/**
 * Class that provides JUnit tests for Gitlet, as well as a couple of utility
 * methods.
 * 
 * @author Joseph Moghadam
 * 
 *         Some code adapted from StackOverflow:
 * 
 *         http://stackoverflow.com/questions
 *         /779519/delete-files-recursively-in-java
 * 
 *         http://stackoverflow.com/questions/326390/how-to-create-a-java-string
 *         -from-the-contents-of-a-file
 * 
 *         http://stackoverflow.com/questions/1119385/junit-test-for-system-out-
 *         println
 * 
 */
public class GitletTest {
	private static final String GITLET_DIR = ".gitlet/";
	private static final String TESTING_DIR = "test_files/";

	/* matches either unix/mac or windows line separators */
	private static final String LINE_SEPARATOR = "\r\n|[\r\n]";

	/**
	 * Deletes existing gitlet system, resets the folder that stores files used
	 * in testing.
	 * 
	 * This method runs before every @Test method. This is important to enforce
	 * that all tests are independent and do not interact with one another.
	 */
	@Before
	public void setUp() {
		File f = new File(GITLET_DIR);
		if (f.exists()) {
			try {
				recursiveDelete(f);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		f = new File(TESTING_DIR);
		if (f.exists()) {
			try {
				recursiveDelete(f);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		f.mkdirs();
	}

	/**
	 * Tests that init creates a .gitlet directory. Does NOT test that init
	 * creates an initial commit, which is the other functionality of init.
	 */
	/*@Test
	public void testBasicInitialize() {
		gitletFast("init");
		File f = new File(GITLET_DIR);
		assertTrue(f.exists());
	}*/

	/**
	 * Test add method
	 */
	/*@Test
	public void testBasicAdd() {
		String wugFileName = TESTING_DIR + "wug.txt";
		String wugText = "This is a wug.";
		createFile(wugFileName, wugText);
		gitletFast("init");
		String stdout = gitletFast("add", "wug2.txt");
		assertTrue(stdout.contains("Add: File does not exist."));

		gitletFast("add", wugFileName);
		stdout = gitletFast("status");
		assertTrue(stdout.contains(wugFileName));
	}*/

	/**
	 * Test initial commit
	 */
	/*@Test
	public void testBasicInitCommit() {
		gitletFast("init");
		String commitMsg = "initial commit";

		String log = gitletFast("log");
		assertArrayEquals(new String[] { commitMsg }, extractCommitMessages(log));
	}*/

	/**
	 * Test Commit method
	 */
	/*@Test
	public void testBasicCommit() {
		gitletFast("init");

		// Check failure cases
		String stdout = gitletFast("commit");
		assertTrue(stdout.contains("Please enter a commit message."));
		stdout = gitletFast("commit", "second commit.");
		assertTrue(stdout.contains("No changes added to the commit."));

		String wugFilename = TESTING_DIR + "wug.txt";
		String wugText = "This is a wug.";
		createFile(wugFilename, wugText);
		gitletFast("add", wugFilename);
		gitletFast("commit", "second commit.");
		stdout = gitletFast("log");
		assertTrue(stdout.contains("second commit."));
	}*/

	/**
	 * Test remove method
	 */
	/*@Test
	public void testBasicRemove() {
		gitletFast("init");
		String wugFilename = TESTING_DIR + "wug.txt";
		String wugText = "This is a wug.";
		createFile(wugFilename, wugText);

		// If the file has been staged and removed ==> unstaged
		gitletFast("add", wugFilename);
		gitletFast("rm", wugFilename);
		String stdout = gitletFast("status");
		assertTrue(!stdout.contains(wugFilename));

		// If the file has been staged, committed and removed
		gitletFast("add", wugFilename);
		gitletFast("commit", "commit wug file.");
		gitletFast("rm", wugFilename);
		stdout = gitletFast("status");
		assertTrue(stdout.contains(wugFilename));
	}*/

	/**
	 * Test branch method
	 */
	/*@Test
	public void testBasicBranch() {
		gitletFast("init");
		gitletFast("branch", "coolBeans");
		String stdOut = gitletFast("status");
		assertTrue(stdOut.contains("coolBeans"));

		String wugFileName = TESTING_DIR + "wug.txt";
		String wugText = "This is a wug.";
		createFile(wugFileName, wugText);
		gitletFast("add", wugFileName);
		gitletFast("commit", "added wug file");
		gitletFast("branch", "coolBeans2");
		stdOut = gitletFast("status");
		assertTrue(stdOut.contains("coolBeans2"));
	}*/

	/**
	 * Test global log
	 */
	/*@Test
	public void testGlobalLog() {
		gitletFast("init");
		String commitMessage1 = "initial commit";

		String wugFileName = TESTING_DIR + "wug.txt";
		String wugText = "This is a wug.";
		createFile(wugFileName, wugText);
		gitletFast("add", wugFileName);
		String commitMessage2 = "added wug";
		gitletFast("commit", commitMessage2);

		String logContent = gitletFast("global-log");
		for (String str : extractCommitMessages(logContent)) {
			assertTrue(Arrays.asList(new String[] { commitMessage2, commitMessage1 }).contains(str));
		}
	}*/

	/*@Test
	public void testGlobalLog2() {
		gitletFast("init");
		String commitMessage1 = "initial commit";
		String wugFileName = TESTING_DIR + "wug.txt";
		String wugText = "This is a wug.";
		createFile(wugFileName, wugText);
		gitletFast("add", wugFileName);
		String commitMessage2 = "added wug.txt";
		gitletFast("commit", commitMessage2);

		gitletFast("branch", "dev");
		gitletFast("checkout", "dev");
		writeFile(wugFileName, "modified in the other branch");
		gitletFast("add", wugFileName);
		String commitMessage3 = "modified wug.txt in dev branch";
		gitletFast("commit", commitMessage3);

		gitletFast("checkout", "master");
		writeFile(wugFileName, "modified in master branch");
		gitletFast("add", wugFileName);
		String commitMessage4 = "modified wug.txt in master branch";
		gitletFast("commit", commitMessage4);

		String stdout;
		stdout = gitletFast("global-log");
		gitletFast("status");
		assertTrue(stdout.contains(commitMessage3));
	}*/

	/**
	 * Tests that checking out a file name will restore the version of the file
	 * from the previous commit. Involves init, add, commit, and checkout.
	 */
	/*@Test
	public void testBasicCheckout() {
		String wugFileName = TESTING_DIR + "wug.txt";
		String wugText = "This is a wug.";
		createFile(wugFileName, wugText);
		gitletFast("init");
		gitletFast("add", wugFileName);
		gitletFast("commit", "added wug");
		writeFile(wugFileName, "This is not a wug.");
		// wugFileName: "This is not a wug." ==> checkout(overwrite with the commit's content) ==> wugFileName: "This is a wug"
		gitletFast("checkout", wugFileName);
		assertEquals(wugText, getText(wugFileName));
	}*/

	/**
	 * Tests that log after one commit conforms to the format in the spec.
	 * Involves init, add, commit, and log.
	 */
	/*@Test
	public void testBasicLog() {
		gitletFast("init");
		String commitMessage1 = "initial commit";

		String wugFileName = TESTING_DIR + "wug.txt";
		String wugText = "This is a wug.";
		createFile(wugFileName, wugText);
		gitletFast("add", wugFileName);
		String commitMessage2 = "added wug";
		gitletFast("commit", commitMessage2);

		String logContent = gitletFast("log");
		assertArrayEquals(new String[] { commitMessage2, commitMessage1 },
				extractCommitMessages(logContent));
	}*/

	/*@Test
	public void testBasicFind() {
		gitletFast("init");
		String wugFileName = TESTING_DIR + "wug.txt";
		String wugText = "This is a wug.";
		createFile(wugFileName, wugText);
		gitletFast("add", wugFileName);
		String commitMessage2 = "added wug.txt";
		gitletFast("commit", commitMessage2); //1

		gitletFast("branch", "dev");
		gitletFast("checkout", "dev");
		writeFile(wugFileName, "modified in the other branch");
		gitletFast("add", wugFileName);
		String commitMessage3 = "modified wug.txt in dev branch";
		gitletFast("commit", commitMessage3); //2

		gitletFast("checkout", "master");
		writeFile(wugFileName, "modified in master branch");
		gitletFast("add", wugFileName);
		String commitMessage4 = "modified wug.txt in master branch";
		gitletFast("commit", commitMessage4); //3

		String stdout;
		stdout = gitletFast("find", "added wug.txt");
		assertEquals(stdout, "1");
		stdout = gitletFast("find", "modified wug.txt in dev branch");
		assertEquals(stdout, "2");
	}*/

	/*@Test
	public void testBasicMerge() {
		gitletFast("init");
		String wugFileName = TESTING_DIR + "wug.txt";
		String wugTextMaster = "This is a wg";
		createFile(wugFileName, wugTextMaster);
		gitletFast("add", wugFileName);
		gitletFast("commit", "added wug file - master");

		wugTextMaster = "This is a wug";
		writeFile(wugFileName, wugTextMaster);
		gitletFast("add", wugFileName);
		gitletFast("commit", "changed wug file - master");

		gitletFast("branch", "dev");
		gitletFast("checkout", "dev");
		String wugTextDev = "This is a wug!";
		writeFile(wugFileName, wugTextDev);
		gitletFast("add", wugFileName);
		String commitMessage3 = "modified wug.txt - dev";
		gitletFast("commit", commitMessage3);
		wugTextDev = "This is a wug!!!!";
		writeFile(wugFileName, wugTextDev);
		gitletFast("add", wugFileName);
		gitletFast("commit", "modified 2 times wug.txt  - dev");
		//gitletFast("checkout", "master"); // -- case1
		String[] args = new String[2];
		args[0] = "merge";
		//args[1] = "dev"; // -- case1
		args[1] = "master"; // -- case2
		Gitlet.main(args);
		//assertEquals(wugTextMaster, getText(wugFileName)); // -- case1
		assertEquals(wugTextDev, getText(wugFileName)); // -- case2
	}

	@Test
	public void testMergeConflict() {
		gitletFast("init");
		String wugFileName = "wug.txt";
		String wugText = "This is the original wug.";
		createFile(wugFileName, wugText);
		gitletFast("add", wugFileName);
		gitletFast("commit", "added wug");

		gitletFast("branch", "dev");

		String wugTextInMaster = "Version of Master.";
		writeFile(wugFileName, wugTextInMaster);
		gitletFast("add", wugFileName);
		gitletFast("commit", "modify wug in master!!");

		gitletFast("checkout", "dev");
		String wugTextInDev = "Version of Dev.";
		writeFile(wugFileName, wugTextInDev);
		gitletFast("add", wugFileName);
		gitletFast("commit", "modify wug in dev!!");

		gitletFast("merge", "master");
		assertEquals(wugTextInDev, getText(wugFileName));
		assertEquals(wugTextInMaster, getText(wugFileName + ".conflicted"));
	}*/

	@Test
	public void testReset() {
		gitletFast("init");
		String wugFilename = "wug.txt";
		createFile(wugFilename, "This is a wug");
		gitletFast("add", wugFilename);
		gitletFast("commit", "1st commit - master"); // id: 1
		writeFile(wugFilename, "This is a wug!!");
		gitletFast("add", wugFilename);
		gitletFast("commit", "2nd commit - master"); // id: 2
		gitletFast("log");
		//gitletFast("reset", "3"); // should print error
		gitletFast("reset", "1");
		assertEquals("This is a wug", getText(wugFilename));
	}


	/**
	 *                        master											master				*dev
	 * (This is a wug)<-(This is a wug...) == Rebase ==> (This is a wug)<-(This is a wug...)<-(This is a wug!!!)
	 * 				  \_(This is a wug!!!)
	 * 				          *dev
	 */
	@Test
	public void testRebase1() {
		gitletFast("init");
		String wugFilename = "wug.txt";
		createFile(wugFilename, "This is a wug");
		gitletFast("add", wugFilename);
		gitletFast("commit", "1st commit - master"); // 1
		gitletFast("branch", "dev");
		writeFile(wugFilename, "This is a wug...");
		gitletFast("add", wugFilename);
		gitletFast("commit", "2nd commit - master"); // 2
		gitletFast("checkout", "dev");
		writeFile(wugFilename, "This is a wug!!!");
		gitletFast("add", wugFilename);
		gitletFast("commit", "1st commit - dev"); // 3
		gitletFast("rebase", "master");
		gitletFast("log");
		assertEquals("This is a wug!!!", getText(wugFilename));
	}

	@Test
	public void testRebase2() {
		gitletFast("init"); // current = master
		String wugFilename = "wug.txt";
		createFile(wugFilename, "This is a wug");
		gitletFast("add", wugFilename);
		gitletFast("commit", "1st commit - master");
		gitletFast("branch", "branch1");
		gitletFast("checkout", "branch1"); // current = branch1
		writeFile(wugFilename, "This is a wug!");
		gitletFast("add", wugFilename);
		gitletFast("commit", "1st commit - branch1");
		gitletFast("branch", "branch2");
		gitletFast("checkout", "branch2"); // current = branch2
		writeFile(wugFilename, "This is a wug???");
		gitletFast("add", wugFilename);
		gitletFast("commit", "1st commit - branch2");
		gitletFast("checkout", "branch1"); // current = branch1
		writeFile(wugFilename, "This is a wug!!!");
		gitletFast("add", wugFilename);
		gitletFast("commit", "2nd commit - branch1");
		gitletFast("checkout", "master"); // current = master
		writeFile(wugFilename, "This is a wug...");
		gitletFast("add", wugFilename);
		gitletFast("commit", "2nd commit - master");
		gitletFast("checkout", "branch1"); // current = branch1
		gitletFast("rebase", "master");
		assertEquals("This is a wug!!!", getText(wugFilename));
		//gitletFast("checkout", "branch2"); // current = branch2
		//gitletFast("This is a wug???", getText(wugFilename));
	}

	@Test
	public void testRebase3() {
		gitletFast("init");
		createFile("a.txt", "a");
		createFile("b.txt", "b");
		createFile("c.txt", "c");
		gitletFast("add", "a.txt");
		gitletFast("add", "b.txt");
		gitletFast("add", "c.txt");
		gitletFast("commit", "splitpoint");
		gitletFast("branch", "abranch");
		writeFile("a.txt", "a'");
		gitletFast("add", "a.txt");
		gitletFast("commit", "master1");
		writeFile("b.txt", "b'");
		gitletFast("add", "b.txt");
		gitletFast("commit", "master2");
		gitletFast("checkout", "abranch");
		writeFile("a.txt", "a1");
		writeFile("c.txt", "c1");
		gitletFast("add", "a.txt");
		gitletFast("add", "c.txt");
		gitletFast("commit", "branch1");
		writeFile("b.txt", "b1");
		writeFile("c.txt", "c2");
		gitletFast("add", "b.txt");
		gitletFast("add", "c.txt");
		gitletFast("commit", "branch2");
		gitletFast("rebase", "master");
	}

	/**
	 * Calls a gitlet command using the terminal.
	 * 
	 * Warning: Gitlet will not print out anything _while_ it runs through this
	 * command, though it will print out things at the end of this command. It
	 * will also return this as a string.
	 * 
	 * The '...' syntax allows you to pass in an arbitrary number of String
	 * arguments, which are packaged into a String[].
	 */
	private static String gitlet(String... args) {
		String[] commandLineArgs = new String[args.length + 2];
		commandLineArgs[0] = "java";
		commandLineArgs[1] = "Gitlet";
		for (int i = 0; i < args.length; i++) {
			commandLineArgs[i + 2] = args[i];
		}
		String results = command(commandLineArgs);
		System.out.println(results);
		return results.trim();
	}

	/**
	 * Another convenience method for calling Gitlet's main. This calls Gitlet's
	 * main directly, rather than through the terminal. This is slightly
	 * cheating the concept of end-to-end tests. But, this allows you to
	 * actually use the debugger during the tests, which you might find helpful.
	 * It's also a lot faster.
	 * 
	 * Warning: Like the other version of this method, Gitlet will not print out
	 * anything _while_ it runs through this command, though it will print out
	 * things at the end of this command. It will also return what it prints as
	 * a string.
	 */
	private static String gitletFast(String... args) {
		PrintStream originalOut = System.out;
		ByteArrayOutputStream printingResults = new ByteArrayOutputStream();
		try {
			/*
			 * Below we change System.out, so that when you call
			 * System.out.println(), it won't print to the screen, but will
			 * instead be added to the printingResults object.
			 */
			System.setOut(new PrintStream(printingResults));
			Gitlet.main(args);
		} finally {
			/*
			 * Restores System.out (So you can print normally).
			 */
			System.setOut(originalOut);
		}
		System.out.println(printingResults.toString());
		return printingResults.toString().trim();
	}

	/**
	 * Returns the text from a standard text file.
	 */
	private static String getText(String fileName) {
		try {
			byte[] encoded = Files.readAllBytes(Paths.get(fileName));
			return new String(encoded, StandardCharsets.UTF_8);
		} catch (IOException e) {
			return "";
		}
	}

	/**
	 * Creates a new file with the given fileName and gives it the text
	 * fileText.
	 */
	private static void createFile(String fileName, String fileText) {
		File f = new File(fileName);
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		writeFile(fileName, fileText);
	}

	/**
	 * Replaces all text in the existing file with the given text.
	 */
	private static void writeFile(String fileName, String fileText) {
		FileWriter fw = null;
		try {
			File f = new File(fileName);
			fw = new FileWriter(f, false);
			fw.write(fileText);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Deletes the file and all files inside it, if it is a directory.
	 * 
	 * @throws IOException
	 */
	private static void recursiveDelete(File d) throws IOException {
		if (d.isDirectory()) {
			for (File f : d.listFiles()) {
				recursiveDelete(f);
			}
		}
		if (!d.delete()) {
			throw new IOException("Failed to delete file " + d.getPath());
		}
	}

	/**
	 * Returns an array of commit messages associated with what log has printed
	 * out.
	 */
	private static String[] extractCommitMessages(String logOutput) {
		String[] logChunks = logOutput.split("===");
		int numMessages = logChunks.length - 1;
		String[] messages = new String[numMessages];
		for (int i = 0; i < numMessages; i++) {
			String[] logLines = logChunks[i + 1].split(LINE_SEPARATOR);
			messages[i] = logLines[3];
		}
		return messages;
	}

	/**
	 * Executes the given command on the terminal, and return what it prints out
	 * as a string.
	 * 
	 * Example: If you want to call the terminal command `java Gitlet add
	 * wug.txt` you would call the method like so: `command("java", "Gitlet",
	 * "add", "wug.txt");` The `...` syntax allows you to pass in however many
	 * strings you want.
	 */
	private static String command(String... args) {
		try {
			StringBuilder results = new StringBuilder();
			Process p = Runtime.getRuntime().exec(args);
			p.waitFor();
			try (BufferedReader br = new BufferedReader(new InputStreamReader(
					p.getInputStream()));) {
				String line = null;
				while ((line = br.readLine()) != null) {
					results.append(line).append(System.lineSeparator());
				}
				return results.toString();
			}
		} catch (IOException e) {
			return e.getMessage();
		} catch (InterruptedException e) {
			return e.getMessage();
		}
	}
}