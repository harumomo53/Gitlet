import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

/**
 * Created by EunjinCho on 2016. 5. 11..
 */
public class CommitNode implements Serializable {
    private transient int nodeId;
    private String nodeMsg;
    protected SimpleDateFormat nodeTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private Date nodeTime;
    private CommitNode nodeParent;
    // map of files (key: filename)
    private HashSet<String> nodeFilesNameSet;


    // for initial commit constructor
    public CommitNode() {
        nodeId = 0;
        nodeMsg = "initial commit";
        nodeTime = new Date();
        nodeParent = null;
        nodeFilesNameSet = new HashSet<>();
    }

    public CommitNode(int nodeId, String nodeMsg, Date nodeTime) {
        this.nodeId = nodeId;
        this.nodeMsg = nodeMsg;
        this.nodeTime = nodeTime;
        nodeFilesNameSet = new HashSet<>();
    }

    public CommitNode(Integer id, String msg, HashSet<String> files) {
        this.nodeId = id;
        this.nodeMsg = msg;
        this.nodeFilesNameSet = files;
        this.nodeTime = new Date();
    }

    /** Setter **/
    public void setNodeParent(CommitNode nodeParent) {
        this.nodeParent = nodeParent;
    }
    public void addNodeFilesName(String filename) {
        this.nodeFilesNameSet.add(filename);
    }
    public void setNodeTime(Date nodeTime) {
        this.nodeTime = nodeTime;
    }
    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    /** Getter **/
    public int getNodeId() {
        return nodeId;
    }
    public String getNodeMsg() {
        return nodeMsg;
    }
    public Date getNodeTime() {
        return nodeTime;
    }
    public HashSet<String> getNodeFilesNameSet() {
        return nodeFilesNameSet;
    }
    public CommitNode getNodeParent() {
        return nodeParent;
    }
}