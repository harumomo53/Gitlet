import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.*;

/**
 * Created by EunjinCho on 2016. 5. 10..
 */
public class Gitlet implements Serializable{
    // ----------------------------- Files  ----------------------------
    private HashSet<String> untrackedFiles;
    private HashSet<String> stagedFiles;
    // -------------------- Record of all branches  --------------------
    // key: branch name, value: commitNode ID
    private HashMap<String, Integer> allBranches;
    // currently active branch name
    private String currentlyActiveBranchName;
    // -------------------- Record of Commit Tree  ---------------------
    private static HashMap<Integer, CommitNode> commitTreeMap;
    // -------------------------- Folder path  -------------------------
    private static final String GIT_DIR = ".gitlet/";
    private String COMMIT_DIR = GIT_DIR + "commits/"; // = .gitlet/commits/
    private String STAGED_DIR = GIT_DIR + "staged/"; // = .gitlet/staged/
    private int curNodeIdSoFar = 0;

    private boolean isConflicted = false;

    public Gitlet() {
        untrackedFiles = new HashSet<>();
        stagedFiles = new HashSet<>();
        // Set branch
        allBranches = new HashMap<>();
        commitTreeMap = new HashMap<>();
    }

    /**
     * Get Back the object saved under given filename.ser
     */
    public static Gitlet deserialize(String filename) {
        Gitlet gitToRtn = null;
        File myGitFile = new File(filename);
        if (myGitFile.exists()) {
            try {
                FileInputStream fileIn = new FileInputStream(filename);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                gitToRtn = (Gitlet) in.readObject();
                in.close();
                fileIn.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                System.out.println("FileInputStream not found.");
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("ObjectInputStream not working.");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                System.out.println("Gitlet class object not found while loading the file.");
            }
        }
        return gitToRtn;
    }


    /**
     * Save the object under given filename.ser
     */
    private static void serialize(Gitlet myGit, String filename) {
        if (myGit == null) {
            return;
        }
        try {
            File myGitFile = new File(filename);
            FileOutputStream fileOut = new FileOutputStream(myGitFile);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(myGit);
            out.close();
            fileOut.close();
        } catch (FileNotFoundException e) {
            System.out.println("fileOutputStream not found.");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("ObjectOutputStream not working.");
            e.printStackTrace();
        }
    }

    /** =============================== Version Control functions =============================== **/

    private void init() {
        File gitFolder = new File(GIT_DIR);
        if (!gitFolder.exists()) {
            // Create .gitlet/ folder
            gitFolder.mkdirs();

            // initiate new CommitNode that contains no files and has "initial commit" msg
            CommitNode initTreeNode = new CommitNode();
            commitTreeMap.put(initTreeNode.getNodeId(), initTreeNode);

            // Create a "commits" folder add this initial commit
            File initCommitFolder = new File(COMMIT_DIR + initTreeNode.getNodeId());
            initCommitFolder.mkdirs();

            // Create a "stage" folder
            File stagedFolder = new File(STAGED_DIR);
            stagedFolder.mkdirs();

            // Create a branch
            allBranches.put("master", initTreeNode.getNodeId());
            currentlyActiveBranchName = "master";
        }
        else {
            System.err.println("A gitlet version control system already exists in the current directory.");
        }
    }


    private void add(String filename) {
        File fileToBeAdded = new File(filename);
        if (!fileToBeAdded.exists()) {
            System.err.println("Add: File does not exist.");
            return;
        }
        else {
            // if the file had been marked for untracking, unmark it
            if (untrackedFiles.contains(filename)) {
                untrackedFiles.remove(filename);
                return;
            }
            // if not, copy the file and add it to the staging area
            else {
                // Copy file and add it to the staging area
                File from = new File(filename);
                File to = new File(STAGED_DIR + filename);
                if (!to.exists()) {
                    to.mkdirs();
                }
                try {
                    Files.copy(from.toPath(), to.toPath(), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // Also add this filename to stagedFiles for later use in commit
                stagedFiles.add(filename);
            }
        }
    }

    private void commit(String commitMsg) {
        if (stagedFiles.size() == 0) {
            System.err.println("No changes added to the commit.");
            return;
        }
        if (commitMsg.equals("")) {
            System.err.println("Please enter a commit message.");
            return;
        }
        // Create a (new)CommitNode with correct info and add it to commitTreeMap
        curNodeIdSoFar++;
        Date dNow = new Date();
        CommitNode newCommit = new CommitNode(curNodeIdSoFar, commitMsg, dNow);
        // set parent for new CommitNode
        Integer parentCommitID = allBranches.get(currentlyActiveBranchName);
        CommitNode parentCommit = commitTreeMap.get(parentCommitID);
        newCommit.setNodeParent(parentCommit);

        // (new)CommitNode keeps everything from parent (that are not updated and removed)
        if (parentCommit != null) {
            for (String pFileName : parentCommit.getNodeFilesNameSet()) {
                if (!stagedFiles.contains(pFileName) && !untrackedFiles.contains(pFileName)) {
                    newCommit.addNodeFilesName(pFileName);
                    // Copy file and add it to the committing area
                    File from = new File(COMMIT_DIR + parentCommit.getNodeId() + "/" + pFileName);
                    File to = new File(COMMIT_DIR + curNodeIdSoFar + "/" + pFileName);
                    if (!to.exists()) {
                        to.mkdirs();
                    }
                    try {
                        Files.copy(from.toPath(), to.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        // Move files from stagedFile directory to commit directory
        for(String stageFileName : stagedFiles) {
            newCommit.addNodeFilesName(stageFileName);
            File from = new File(STAGED_DIR + stageFileName);
            File to = new File(COMMIT_DIR + curNodeIdSoFar + "/" +stageFileName);
            if (!to.exists()) {
                to.mkdirs();
            }
            try {
                Files.copy(from.toPath(), to.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // The commit just made becomes the current commit, update the allBranches
        allBranches.put(currentlyActiveBranchName, curNodeIdSoFar);

        // reset untrackedFiels/ reset stagedFiles
        untrackedFiles = new HashSet<>();
        stagedFiles = new HashSet<>();

        // Staging area should be cleared
        File dir = new File(STAGED_DIR);
        for (File file : dir.listFiles()) {
            file.delete();
        }

        // Finally add to the tree
        commitTreeMap.put(newCommit.getNodeId(), newCommit);

        isConflicted = false;
    }

    private void remove(String filename) {
        if (!stagedFiles.contains(filename) ||
                !(commitTreeMap.get(allBranches.get(currentlyActiveBranchName)).getNodeFilesNameSet()).contains(filename)) {
            System.err.println("No reason to remove the file.");
        }

        // if the file had been staged, just only unstage it
        if (stagedFiles.contains(filename)) {
            stagedFiles.remove(filename);
            File stageDir = new File(STAGED_DIR);
            for (File f : stageDir.listFiles()) {
                if (f.getName().equals(filename)) {
                    f.delete();
                }
            }
        }
        // mark the file for untracking
        else {
            untrackedFiles.add(filename);
        }
    }

    /**
     * Process:
     *  branch() --> checkout()
     * Branch = creates a new pointer
     * Checkout = switch currently active head pointer
     */
    private void branch(String[] args) {
        String newBranchName = args[1];
        // if a branch with the given name already exists
        if (allBranches.containsKey(newBranchName)) {
            System.err.println("A branch with that name already exists.");
            return;
        }
        if (isConflicted) {
            System.err.println("Cannot do this command until the merge conflict has been resolved.");
            return;
        }
        // have this new branch name point to current head node
        Integer curHeadNodeID = allBranches.get(currentlyActiveBranchName);
        allBranches.put(newBranchName, curHeadNodeID);

    }

    private void checkout(String[] args) {
        Integer useCase = 0;
        Integer curBranchID = allBranches.get(currentlyActiveBranchName);
        CommitNode curBranchNode = commitTreeMap.get(curBranchID);
        // Determine case
        if (args.length == 2) {
            String name = args[1];
            if (!curBranchNode.getNodeFilesNameSet().contains(name)) {
                useCase = 3;
            }
            if (!allBranches.containsKey(name)) {
                useCase = 1;
            }
            // if we have a file name that's the same as a branch name, branch name takes precedence
            if (curBranchNode.getNodeFilesNameSet().contains(name) && allBranches.containsKey(name)) {
                useCase = 3;
            }
        }

        if (args.length == 3) {
            useCase = 2;
        }

        // if (args[1]=filename)
        if (useCase == 1) {
            String filename = args[1];
            // if the file not exist in the head commit
            if (!curBranchNode.getNodeFilesNameSet().contains(filename)) {
                System.err.println("File does not exist in the most recent commit, or no such branch exists 1.");
                return;
            }
            // if so, takes the file and puts it in the working directory, overwriting if necessary
            File from = new File(COMMIT_DIR + curBranchID + "/" + filename);
            File to = new File(filename);
            if (!to.exists()) {
                to.mkdirs();
            }
            try {
                Files.copy(from.toPath(), to.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // if (args[1]=commitID, args[2]=filename)
        if (useCase == 2) {
            Integer commitID = Integer.parseInt(args[1]);
            String filename = args[2];
            CommitNode nodeForGivenID = commitTreeMap.get(commitID);
            // If no commit with the given id exists
            if (!commitTreeMap.containsKey(commitID)) {
                System.err.println("No commit with that id exists.");
                return;
            }
            // if the file does not exist with the given id
            if (!nodeForGivenID.getNodeFilesNameSet().contains(filename)) {
                System.err.println("File does not exist in that commit.");
                return;
            }
            // check if the file exists in the given commitID
            // if so, takes the file and puts it in the working directory, overwriting if necessary
            File from = new File(COMMIT_DIR + commitID + "/" + filename);
            File to = new File(filename);

            if (!to.exists()) {
                to.mkdirs();
            }
            try {
                Files.copy(from.toPath(), to.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // if (args[1]=branchname)
        if (useCase == 3) {
            if (isConflicted) {
                System.err.println("Cannot do this command until the merge conflict has been resolved.");
                return;
            }
            String branchname = args[1];
            // If no branch with that name exists
            if (!allBranches.containsKey(branchname)) {
                System.err.println("File does not exist in the most recent commit, or no such branch exists 2.");
                return;
            }
            // if hat branch is the current branch
            if (branchname.equals(currentlyActiveBranchName)) {
                System.err.println("No need to checkout the current branch.");
                return;
            }
            // take all files in the "head commit" of "branchname"
            // and puts them in the working directory, overwriting if necessary
            Integer headCommitOfGivenBranchID = allBranches.get(branchname);
            CommitNode headCommitOfGivenBranch = commitTreeMap.get(headCommitOfGivenBranchID);
            // Todo : 이게 맞는지 모르겠다. 끝에 /를 붙여야 하나
            File headDir = new File(COMMIT_DIR + headCommitOfGivenBranch.getNodeId());
            for (String filename : headCommitOfGivenBranch.getNodeFilesNameSet()) {
                File from = new File(COMMIT_DIR + headCommitOfGivenBranchID + "/" + filename);
                File to = new File(filename);
                if (!to.exists()) {
                    to.mkdirs();
                }
                try {
                    Files.copy(from.toPath(), to.toPath(), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // the "branchname" is the currentlyActiveBranchName now
            currentlyActiveBranchName = branchname;
        }
    }

    private void merge(String[] args) {
        String givenBranchName = args[1];
        // If a branch with the given name doesn't exist
        if (!allBranches.containsKey(givenBranchName)) {
            System.err.println("A branch with that name does not exist.");
            return;
        }
        // If attempting to merge a branch with itself
        if (givenBranchName.equals(currentlyActiveBranchName)) {
            System.err.println("Cannot merge a branch with itself.");
            return;
        }
        if (isConflicted) {
            System.err.println("Cannot do this command until the merge conflict has been resolved.");
            return;
        }
        // 1. Find split point on the "currentBranch" and the "givenBranch"
        CommitNode currentNode1 = commitTreeMap.get(allBranches.get(currentlyActiveBranchName));
        CommitNode givenNode1 = commitTreeMap.get(allBranches.get(givenBranchName));
        CommitNode givenNodeCopy = givenNode1;
        while (currentNode1.getNodeId() != givenNodeCopy.getNodeId()) {
            givenNodeCopy = givenNodeCopy.getNodeParent();
            if (givenNodeCopy.getNodeParent() == null) {
                givenNodeCopy = givenNode1;
                currentNode1 = currentNode1.getNodeParent();
            }
        }
        CommitNode splitNode = currentNode1;
        String SPLIT_DIR = COMMIT_DIR + splitNode.getNodeId() + "/";
        // ------------------------------------------------------------------------------
        CommitNode currentNode2 = commitTreeMap.get(allBranches.get(currentlyActiveBranchName));
        CommitNode givenNode2 = commitTreeMap.get(allBranches.get(givenBranchName));
        HashSet<String> curModified = new HashSet<>();
        HashSet<String> curNotModified = new HashSet<>();
        HashSet<String> givenModified = new HashSet<>();
        HashSet<String> givenNotModified = new HashSet<>();

        for (String cFile : currentNode2.getNodeFilesNameSet()) {
            // if this file is newly added file for current branch
            if (!splitNode.getNodeFilesNameSet().contains(cFile)) {
                curModified.add(cFile);
            }
            else {
                // check if this file is modified since split point
                File curFile = new File(COMMIT_DIR + currentNode2.getNodeId() + "/" + cFile);
                File splitFile = new File(SPLIT_DIR + cFile);
                // if they're NOT modified
                if (compareTwoFileContents(curFile, splitFile)) {
                    curNotModified.add(cFile);
                }
                else {
                    curModified.add(cFile);
                }
            }
        }

        for (String gFile : givenNode2.getNodeFilesNameSet()) {
            // if this newly added file for given branch
            if (!splitNode.getNodeFilesNameSet().contains(gFile)) {
                givenModified.add(gFile);
            }
            else {
                // check if this file is modified since split point
                File givenFile = new File(COMMIT_DIR + givenNode2.getNodeId() + "/" + gFile);
                File splitFile = new File(SPLIT_DIR + gFile);
                if (compareTwoFileContents(givenFile, splitFile)) {
                    givenNotModified.add(gFile);
                }
                else {
                    givenModified.add(gFile);
                }
            }
        }

        // 2.1.1. Find all files that: "givenBranch" is changed since the split point, but "currentBranch" is NOT changed
        // These files should be changed to their versions in givenBranch
        HashSet<String> shouldBeStagedF = new HashSet<>();
        for (String gModified : givenModified) {
            if (curNotModified.contains(gModified)) {
                shouldBeStagedF.add(gModified);
            }
        }

        // 2.1.2. Stage these files (or mark for untracking if the modification was a removal)
        for (String stageF : shouldBeStagedF) {
            stagedFiles.add(stageF);
        }

        for (String sFile : splitNode.getNodeFilesNameSet()) {
            // it means that given branch removed the file that existed in split point
            if (!givenModified.contains(sFile)) {
                untrackedFiles.add(sFile);
            }
        }

        // ------------------------------------------------------------------------------
        // 2.2.1 If "currentBranch" is changed, but "givenBranch" is NOT changed since the split point,
        // stay as they are (oh. 왜냐면 나중에 commit될때는 parent에서 다 받아오니까? stage 되있는 거 말고)

        // ------------------------------------------------------------------------------
        // 2.3.1 If both "currentBranch" and "givenBranch" are modified, the files should stay as they are
        // 2.3.2 The version of the file from "givenBranch" should be copied into the working dir
        // with the name [old file name].conflicted
        for (String curModifiedF : curModified) {
            if (givenModified.contains(curModifiedF)) {
                File from = new File(COMMIT_DIR + givenNode2.getNodeId() + "/" + curModifiedF);
                File to = new File(curModifiedF + ".conflicted");
                isConflicted = true;
                if (!to.exists()) {
                    to.mkdirs();
                }
                try {
                    Files.copy(from.toPath(), to.toPath(), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // ------------------------------------------------------------------------------
        // 3.1 If merge didn't generate .conflicted files, automatically commit with a msg
        if (!isConflicted) {
            commit("Merged " + currentlyActiveBranchName + " with " + givenBranchName);
        }
        else {
            // 3.2 If merge did generate .conflicted, DO NOT automatically commit
            System.out.println("Encountered a merge conflict.");
        }
    }

    private static boolean compareTwoFileContents(File newlyAddedFile, File existedFile) {
        try {
            return Arrays.equals(Files.readAllBytes(newlyAddedFile.toPath()),
                    Files.readAllBytes(existedFile.toPath()));
        } catch (IOException e) {
            return false;
        }
    }


    /**
     * Snaps the "current branch" and reattaches the "current branch" to the head of "given branch"
     * result : (given branch) <- (current branch)
     *
     * conflict: (Essentially, it should work like this)
     *  - if the given branch file is modified, current branch file is unmodified => files in the given branch should take the place
     *  - if both of the given branch and current branch are modified => conflicted file
     *
     * However, for simplicity
     *  - Just keep the current branch's copies of the files
     */
    private void rebase(String[] args) {
        String givenBranch = args[1];
        // if a givenBranch not exist
        if (!allBranches.containsKey(givenBranch)) {
            System.err.println("A branch with that name does not exist.");
            return;
        }
        // If a givenBranch is the same as the current branch
        if (givenBranch.equals(currentlyActiveBranchName)) {
            System.err.println("Cannot rebase a branch onto itself.");
            return;
        }
        // find the split point of the current branch and the givenBranch
        CommitNode currentNode = commitTreeMap.get(allBranches.get(currentlyActiveBranchName));
        CommitNode givenNode = commitTreeMap.get(allBranches.get(givenBranch));
        CommitNode givenNodeCopy = givenNode;
        while (currentNode.getNodeId() != givenNodeCopy.getNodeId()) {
            givenNodeCopy = givenNodeCopy.getNodeParent();
            if (givenNodeCopy.getNodeParent() == null) {
                givenNodeCopy = givenNode;
                currentNode = currentNode.getNodeParent();
            }
        }
        CommitNode splitNode = currentNode;
        System.out.println("splitnode: " + splitNode.getNodeId());
        String SPLIT_DIR = COMMIT_DIR + splitNode.getNodeId() + "/";
        // If the givenBranch's head is in the history of the current branch's head
        if (splitNode.getNodeId() == givenNode.getNodeId()) {
            System.err.println("Already up-to-date.");
            return;
        }
        CommitNode currentNode2 = commitTreeMap.get(allBranches.get(currentlyActiveBranchName));
        CommitNode givenNode2 = commitTreeMap.get(allBranches.get(givenBranch));
        System.out.println("currentNode2: " + currentNode2.getNodeId() + " givenNode2: " + givenNode2.getNodeId() + " split: " +splitNode.getNodeId());
        // If the current branch's head is in the history of the givenBranch,
        if (splitNode.getNodeId() == currentNode2.getNodeId()) {
            System.out.println("current is in given: " + currentNode2.getNodeId() );
            // just moves the current branch to point to the same commit that the givenBranch points to
            this.allBranches.put(currentlyActiveBranchName, givenNode2.getNodeId());
            String[] args2 = {"reset", Integer.toString(givenNode2.getNodeId())};
            this.reset(args2);
        }
        else {
            System.out.println("else");
            // makes a "copy" of the current branch nodes (replay the branch) with "new" ids and time stamps
            ArrayList<CommitNode> copyList = copyBranch(currentNode2, splitNode);
            // remove the last node (split node)
            copyList.remove(copyList.size() -1);
            // (givenNode) <- (c1)
            copyList.get(copyList.size() - 1).setNodeParent(givenNode);
            System.out.println("copyList: " + copyList.get(copyList.size() - 1).getNodeId() + " has new parent: " + givenNode.getNodeId());
            // moves the branch pointer to point this "copy"
            allBranches.put(currentlyActiveBranchName, copyList.get(0).getNodeId());
            // add commit record to the map
            for (CommitNode each : copyList) {
                commitTreeMap.put(each.getNodeId(), each);
            }
        }
    }

    /**
     * Copy all commitNodes on the branch in between splitNode and currentNode
     * All copies of commit in an order of (currentNode -> splitNode) and inclusive
     */
    private ArrayList<CommitNode> copyBranch(CommitNode currentNode, CommitNode splitNode) {
        System.out.println("copyBranch...");
        ArrayList<CommitNode> copy = new ArrayList<>();
        CommitNode copyNode = new CommitNode();
        CommitNode prevNode = new CommitNode();
        System.out.println();
        System.out.println("curNodeidSofar: " + curNodeIdSoFar);
        while (currentNode.getNodeId() != splitNode.getNodeId()) {
            curNodeIdSoFar += 1;
            copyNode = new CommitNode(curNodeIdSoFar, currentNode.getNodeMsg(), currentNode.getNodeFilesNameSet());
            System.out.println("created new node with id: " + copyNode.getNodeId());
            if (!copy.contains(copyNode)) {
                copy.add(copyNode);
                System.out.println("add that node with id: " + copyNode.getNodeId());
                int curIndex = copy.indexOf(copyNode);
                if (curIndex != 0) {
                    copyNode.setNodeParent(copy.get(curIndex-1));
                    System.out.println("copyNode's parent: " + copy.get(curIndex-1).getNodeId());
                }
                // copy contents
                for (String fName : currentNode.getNodeFilesNameSet()) {
                    File from = new File(COMMIT_DIR + currentNode.getNodeId() + "/" + fName);
                    File to = new File(COMMIT_DIR + copyNode.getNodeId() + "/" + fName);
                    if (!to.exists()) {
                        to.mkdirs();
                    }
                    try {
                        Files.copy(from.toPath(), to.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            currentNode = currentNode.getNodeParent();
        }
        copy.add(splitNode);
        System.out.println("add split: " + splitNode.getNodeId());
        System.out.println("copylist size: " + copy.size());
        return copy;
    }

    private void reset(String[] args) {
        Integer commitID = Integer.valueOf(args[1]);
        // if no commit with the given id exists
        if (!commitTreeMap.containsKey(commitID)) {
            System.err.println("No commit with that id exists.");
            return;
        }

        // Find the commitNode of the given id
        CommitNode givenCommit = commitTreeMap.get(commitID);

        // Checks out all the files tracked by the given commit
        for (String filename : givenCommit.getNodeFilesNameSet()) {
            File from = new File(COMMIT_DIR + commitID + "/" + filename);
            File to = new File(filename);

            if (!to.exists()) {
                to.mkdirs();
            }
            try {
                Files.copy(from.toPath(), to.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void find(String[] args) {
        String msg = args[1];
        StringBuilder sb = new StringBuilder();
        int count = 0;
        for (Map.Entry<Integer, CommitNode> entry : commitTreeMap.entrySet()) {
            if (entry.getValue().getNodeMsg().equals(msg)) {
                sb.append(entry.getValue().getNodeId());
                sb.append("\n");
                count++;
            }
        }
        if (count == 0) {
            System.err.println("Found no commit with that message.");
            return;
        }
        System.out.println(sb.toString());
    }

    private void status() {
        StringBuilder sb = new StringBuilder();
        sb.append("=== Branches ===");
        sb.append("\n");
        for (Map.Entry<String, Integer> entry : allBranches.entrySet()) {
            if (entry.getKey().equals(currentlyActiveBranchName)) {
                sb.append("*");
                sb.append(entry.getKey());
                sb.append("\n");
            }
            else {
                sb.append(entry.getKey());
                sb.append("\n");
            }
        }
        sb.append("=== Staged Files ===");
        sb.append("\n");
        for (String s : stagedFiles) {
            sb.append(s);
            sb.append("\n");
        }
        sb.append("=== Files Marked for Untracking ===");
        sb.append("\n");
        for (String us : untrackedFiles) {
            sb.append(us);
            sb.append("\n");
        }

        System.out.println(sb.toString());
    }


    private void log() {
        StringBuilder sb = new StringBuilder();
        Integer activeHeadNodeID = allBranches.get(currentlyActiveBranchName);
        CommitNode activeHeadNode = commitTreeMap.get(activeHeadNodeID);

        while (activeHeadNode != null) {
            sb.append("===");
            sb.append("\n");
            sb.append("Commit ");
            sb.append(activeHeadNode.getNodeId());
            sb.append("\n");
            sb.append(activeHeadNode.nodeTimeFormat.format(activeHeadNode.getNodeTime()));
            sb.append("\n");
            sb.append(activeHeadNode.getNodeMsg());
            sb.append("\n");
            activeHeadNode = activeHeadNode.getNodeParent();
        }
        System.out.println(sb.toString());
    }

    /**
     * Unlike log, global-log prints all commits made so far
     */
    private void globalLog() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Integer, CommitNode> entry : commitTreeMap.entrySet()) {
            sb.append("===");
            sb.append("\n");
            sb.append("Commit ");
            sb.append(entry.getValue().getNodeId());
            sb.append("\n");
            sb.append(entry.getValue().nodeTimeFormat.format(entry.getValue().getNodeTime()));
            sb.append("\n");
            sb.append(entry.getValue().getNodeMsg());
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }

    private boolean isDangerOk() {
        Scanner check = new Scanner(System.in);
        System.out.println("Warning: This command may potentially overwrites files. Are you sure you want to continue? (yes/no)");
        String input = check.nextLine();
        if (input.equals("yes")) {
            return true;
        }
        return false;
    }


    public static void main(String[] args) {
        Gitlet git;
        // Retrieve what I've saved so far in myGit.ser
        if (Gitlet.deserialize(GIT_DIR + "myGit.ser") == null) {
            git = new Gitlet();
        }
        else {
            git = Gitlet.deserialize(GIT_DIR + "myGit.ser");
        }
        //System.out.println("args2: " + args[0]);
        switch (args[0]) {
            case "init":
//                System.out.println("init");
                git.init();
                break;

            case "add":
//                System.out.println("add");
                git.add(args[1]);
                break;

            case "commit":
                if (args.length != 2) {
                    System.out.println("Please enter a commit message.");
                } else {
                    git.commit(args[1]);
                }
                break;

            case "rm":
                if (args.length != 2) {
                    System.out.println("Please enter a file name to remove.");
                } else {
                    git.remove(args[1]);
                }
                break;

            case "checkout":
                git.checkout(args);
                break;

            case "branch":
                git.branch(args);
                break;

            case "merge":
                git.merge(args);
                break;

            case "rebase":
                git.rebase(args);
                break;

            case "reset":
                git.reset(args);
                break;

            case "find":
                git.find(args);
                break;

            case "status":
//                System.out.println("status");
                git.status();
                break;

            case "log":
//                System.out.println("log");
                git.log();
                break;

            case "global-log":
                git.globalLog();
                break;

            default:
                break;
        }
        Gitlet.serialize(git, ".gitlet/myGit.ser");
    }

}
